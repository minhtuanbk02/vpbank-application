Cac buoc thực hiện

Bước 1: Backup database phiên bản 2.6.0 (apimgtdb)
Bước 2: Backupda database phiên bản 4.1.0 (cả 2 database apim và shared)
Chú ý:  nên (import database ở bước 1 lên cum database mới để thực hiện migrate)
Bước 3: Chỉnh sửa thông số cấu hình của các database trong thư mục application-dev.properties (url, username, password)
Bước 4: Chỉnh sửa các thông số cấu hình
 - dateStart: lấy danh sách các application từ thời điểm này (vd: 2023-09-16 10:00:00 ) (yyyy-MM-dd HH:mm:ss)
 - appName: có 1 Application đã bị xóa nhầm, sau đó tạo lại một app mới và có update value consumerKey + consumerScretkey) 
 cần bỏ qua app mới này trong quá trình migrate khi app bị xóa ở bản cũ đã có trên bản 4.1.0)
 - keyManager: trường UUID được lấy từ table AM_KEY_MANAGER trên bản 4.1.0
Bước 5: Chỉnh sửa filename trong file log4j2.properties:
 - property.filename: Đường dẫn ghi ra file log trong quá trình migrate application
Bước 6: build thành gói jar và đưa lên máy chủ (máy chủ có thông kết nối với các database ở bước 1 và bước 2)
   
Bước 7: Chạy câu lệnh java -jar ./tên gói jar
Bước 8: Xem kết quả migrate trên màn hình console hoặc file log
Bước 9: Test application đã migrate bằng việc (quan sát trên giao diện devportal + call api lấy danh sách token)

Danh sách các bảng được import khi migrate 1 application và consumer key + consumer secret:
Khi tạo application
1. am_application
   
Khi gen cặp key:
2. am_application_key_mapping
3. am_application_registration
4. idn_oauth_consumer_apps
5. idn_oauth2_access_token
6. idn_oauth2_access_token_scope
7. idn_oidc_property
8. idn_scim_group
9. sp_app
10. sp_inbound_auth
11. sp_metadata

Có table trên database shared
12. um_hybrid_role
13. um_hybrid_user_role
