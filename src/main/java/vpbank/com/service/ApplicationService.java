package vpbank.com.service;

import vpbank.com.application26.entity.AmApplication;

import java.util.List;

public interface ApplicationService {
    List<AmApplication> migrateApplication (String date, String appName);
}
