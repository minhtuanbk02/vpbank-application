package vpbank.com.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vpbank.com.application26.entity.*;
import vpbank.com.application26.repository.*;
import vpbank.com.application41.entity.*;
import vpbank.com.application41.repository.*;
import vpbank.com.service.ApplicationService;
import vpbank.com.shared41.entity.UmHybridRole;
import vpbank.com.shared41.entity.UmHybridUserRole;
import vpbank.com.shared41.repository.UmHybridRoleRepository;
import vpbank.com.shared41.repository.UmHybridUserRoleRepository;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
@Service
@Transactional(rollbackFor = Exception.class)
public class ApplicationServiceImpl implements ApplicationService {
    private static final Logger logger = LogManager.getLogger(ApplicationServiceImpl.class);

    @Autowired private Environment env;

    @Autowired private AmApplicationRepository amApplicationRepository;

    @Autowired private AmApplication41Repository amApplication41Repository;

    @Autowired private SubscriberRepository subscriberRepository;

    @Autowired private Subscriber41Repository subscriber41Repository;

    @Autowired private ConsumerAppsRepository consumerAppsRepository;

    @Autowired private ConsumerApps41Repository consumerApps41Repository;

    @Autowired private IdbOauth2AccessTokenRepository idbOauth2AccessTokenRepository;

    @Autowired private IdbOauth2AccessToken41Repository idbOauth2AccessToken41Repository;

    @Autowired private ApplicationRegistrationRepository applicationRegistrationRepository;

    @Autowired private ApplicationRegistration41Repository applicationRegistration41Repository;

    @Autowired private IdnOidcProperty41Repository  idnOidcProperty41Repository;

    @Autowired private SpAppRepository spAppRepository;

    @Autowired private SpApp41Repository spApp41Repository;

    @Autowired private SpInboundAuthRepository spInboundAuthRepository;

    @Autowired private SpInboundAuth41Repository spInboundAuth41Repository;

    @Autowired private SpMetadataRepository spMetadataRepository;

    @Autowired private SpMetadata41Repository spMetadata41Repository;

    @Autowired private IdnScimGroupRepository idnScimGroupRepository;

    @Autowired private IdnScimGroup41Repository idnScimGroup41Repository;

    @Autowired private UmHybridRoleRepository umHybridRoleRepository;

    @Autowired private UmHybridUserRoleRepository umHybridUserRoleRepository;

    @Autowired private SubscriptionRepository subscriptionRepository;

    @Autowired private Subscription41Repository subscription41Repository;

    @Autowired private AmApiRepository amApiRepository;

    @Autowired private AmApi41Repository amApi41Repository;

    @Override
    public List<AmApplication> migrateApplication(String date, String appNameEdit) {
        List<AmApplication> listApp;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = new Date();
        try {
            startDate = df.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        listApp = amApplicationRepository.findAmApplicationByCreatedTimeGreaterThanEqual(startDate);
        if (listApp!= null && listApp.size() >0){
            for (AmApplication app26 : listApp) {
                try{
                    //remove app xoa nham va phai tao lai
                    if (app26.getName().contains(appNameEdit)){
                        continue;
                    }
                    //check app exit tren ban 4.1.0
                    List<AmApplication41> checkExit = amApplication41Repository.findAmApplication41sByNameAndCreatedBy(app26.getName(), app26.getCreatedBy());
                    if(checkExit != null && checkExit.size() >0){
                        //check exits consumer apps
                        AmApplication41 app41 = checkExit.get(0);
                        String name = app41.getName();
                        String username = app41.getCreatedBy();
                        String appNamePro = username.concat("_").concat(name).concat("_PRODUCTION");
                        String appNameSan = username.concat("_").concat(name).concat("_SANDBOX");
                        List <ConSumerApps> listPro26 = consumerAppsRepository.findAllByAppNameAndUserName(appNamePro, username);
                        List <ConSumerApps> listSan26 = consumerAppsRepository.findAllByAppNameAndUserName(appNameSan, username);
                        if (listPro26.size() > 0 || listSan26.size() > 0){
                            List <ConSumerApps41> listPro41 = consumerApps41Repository.findAllByAppNameAndUserName(appNamePro, username);
                            List <ConSumerApps41> listSan41 = consumerApps41Repository.findAllByAppNameAndUserName(appNameSan, username);
                            if(listPro41.size()==0 || listSan41.size()==0){
                                logger.info("-----start migrate ConsumerAppsWithAppOld----. "+app41.getName()+"_Username: " +app26.getCreatedBy());
                                migarateConsumerApps(app41, app26);
                                logger.info("-----done migrate ConsumerAppsWithAppOld----. "+app41.getName() +"_Username: " +app26.getCreatedBy());
                            }
                        }
                        continue;
                    }else {
                        logger.info("---------Start migrate AppName: " + app26.getName() +"_CreateBy: " +app26.getCreatedBy());
                        AmApplication41 amApplication41 = new AmApplication41();
                        amApplication41.setName(app26.getName());
                        //check Subscriber chua co thi them vao
                        CheckSucriber(app26.getSubscriberId());
                        amApplication41.setSubscriberId(CheckSucriber(app26.getSubscriberId()));
                        amApplication41.setApplicationTier(app26.getApplicationTier());
                        amApplication41.setCallbackUrl(app26.getCallbackUrl());
                        amApplication41.setDescription(app26.getDescription());
                        amApplication41.setApplicationStatus(app26.getApplicationStatus());
                        amApplication41.setGroupId(app26.getGroupId());
                        amApplication41.setCreatedBy(app26.getCreatedBy());
                        amApplication41.setCreatedTime(app26.getCreatedTime());
                        amApplication41.setUpdateBy(app26.getUpdateBy());
                        amApplication41.setUpdatedTime(app26.getUpdatedTime());
                        amApplication41.setTokenType(app26.getTokenType());
                        amApplication41.setUuid(app26.getUuid());
                        amApplication41.setOrganization("carbon.super");
                        amApplication41Repository.save(amApplication41);
                        AmApplication41 application41 = amApplication41Repository.findFirstByNameAndCreatedBy(app26.getName(), app26.getCreatedBy());
                        //migrate table idn_oauth_consumer_apps
                        migarateConsumerApps (application41, app26);
                        migrateSubcription(app26.getApplicationId(),application41.getApplicationId());
                        logger.info("---------done migrate AppName: " + app26.getName() +"_CreateBy: " +app26.getCreatedBy());
                    }
                }catch (Exception e){
                    logger.info("----error migrate App---: "+app26.getName()+"_Username: " +app26.getCreatedBy().concat("_ Message: "+e.getMessage()));
                }
            }
        }
        return listApp;
    }

    private int CheckSucriber(int subcriber26) {
        int subcriberId;
        String userId = null;
        Subcriber subcriber = subscriberRepository.findSubcriberBySubscriberId(subcriber26);
        if (subcriber != null){
            userId = subcriber.getUserId();
        }
        //check user đã có tồn tại trên bản 4.1.0
        List<Subcriber41> listUser = subscriber41Repository.findAllByUserId(userId);
        if (listUser != null && listUser.size() >0){
            Subcriber41 subcriber41 = listUser.get(0);
            subcriberId = subcriber41.getSubscriberId();
            return subcriberId;
        }else {
           //Chưa có thì thêm vào
            Subcriber41 subcriber41 = new Subcriber41();
            subcriber41.setUserId(subcriber.getUserId());
            subcriber41.setEmailAddress(subcriber.getEmailAddress());
            subcriber41.setTenantId(subcriber.getTenantId());
            subcriber41.setDateSubscribed(subcriber.getDateSubscribed());
            subcriber41.setCreatedBy(subcriber.getCreatedBy());
            subcriber41.setCreatedTime(subcriber.getCreatedTime());
            subcriber41.setUpdateBy(subcriber.getUpdateBy());
            subcriber41.setUpdatedTime(subcriber.getUpdatedTime());
            subscriber41Repository.save(subcriber41);
            List<Subcriber41> listUser2 = subscriber41Repository.findAllByUserId(subcriber.getUserId());
            Subcriber41 sub = listUser2.get(0);
            subcriberId = sub.getSubscriberId();
            return subcriberId;
        }
    }

    private void migarateConsumerApps(AmApplication41 application41, AmApplication application26){
        //check exit database 2.6.0
        String appName = application41.getName();
        String username = application41.getCreatedBy();
        String appNamePro = username.concat("_").concat(appName).concat("_PRODUCTION");
        List <ConSumerApps> listPro = consumerAppsRepository.findAllByAppNameAndUserName(appNamePro, username);
        List <ConSumerApps41> listPro41 = consumerApps41Repository.findAllByAppNameAndUserName(appNamePro, username);
        if (listPro != null && listPro.size() >0 && listPro41.size()==0){
            ConSumerApps conSumerApps = listPro.get(0);
            ConSumerApps41 conSumerApps41 = new ConSumerApps41();
            conSumerApps41 = convertConSumerApps(conSumerApps ,conSumerApps41);
            consumerApps41Repository.save(conSumerApps41);
            //migrate idn_oauth2_access_token
            List<ConSumerApps41> list41Pro = consumerApps41Repository.findAllByAppNameAndUserName(appNamePro, username);
            if (list41Pro != null && list41Pro.size() >0){
                logger.info("-----start migrate ConsumerApps----. "+appNamePro+"_Username: " +username);
                ConSumerApps41 conSumerApps41Pro = list41Pro.get(0);
                //migrate idn_oauth2_access_token
                convertIdnOauth2AccessToken(conSumerApps.getId(), conSumerApps41Pro.getUserName(), conSumerApps41.getId());
                //migrate table am_application_key_mapping
                migarateKeyMapping (conSumerApps41.getConsumerKey(), "PRODUCTION", application41);
                //migrate table am_application_registration
                migrateApplicationRegistration(application41, application26);
                //migrate idn_oidc_property
                migrateIdnOidcProduction (conSumerApps41.getConsumerKey());
                //migrate idn_scim_group
                migarateIdnScimGroup(appNamePro);
                //migrate SP_app and SP_Idn_Inbound_Auth and SP_Metadata
                int idSp41Pro = migarateSpApp(appNamePro,username);
                SpApp sp26 = spAppRepository.findFirstByAppNameAndUserName(appNamePro, username);
                migrateIdnInboundAuth(conSumerApps41.getConsumerKey(), idSp41Pro, sp26.getId());
                migrateSpMetadata(sp26.getId(), idSp41Pro);
                migrateUmHybridRole(appNamePro,username);
                logger.info("-----done migrate ConsumerApps----. "+appNamePro+"_Username: " +username);
            }
        }
        String appNameSan = username.concat("_").concat(appName).concat("_SANDBOX");
        List <ConSumerApps> listSan = consumerAppsRepository.findAllByAppNameAndUserName(appNameSan, username);
        List <ConSumerApps41> listSan41 = consumerApps41Repository.findAllByAppNameAndUserName(appNameSan, username);
        if (listSan != null && listSan.size() >0 && listSan41.size() == 0){
            logger.info("-----start migrate ConsumerApps----. "+appNameSan+"_Username: " +username);
            ConSumerApps conSumerApps = listSan.get(0);
            ConSumerApps41 conSumerApps41San = new ConSumerApps41();
            conSumerApps41San = convertConSumerApps(conSumerApps ,conSumerApps41San);
            consumerApps41Repository.save(conSumerApps41San);
            List<ConSumerApps41> list41San = consumerApps41Repository.findAllByAppNameAndUserName(appNameSan, username);
            if (list41San.size() >0){
                ConSumerApps41 conSumerApps41 = list41San.get(0);
                //migrate idn_oauth2_access_token
                convertIdnOauth2AccessToken(conSumerApps.getId(), conSumerApps41.getUserName(), conSumerApps41.getId());
                //migrate table am_application_key_mapping
                migarateKeyMapping (conSumerApps41.getConsumerKey(),"SANDBOX", application41);
                //migrate table am_application_registration
                migrateApplicationRegistration(application41, application26);
                //migrate idn_oidc_property
                migrateIdnOidcProduction (conSumerApps41.getConsumerKey());
                //migrate idn_scim_group
                migarateIdnScimGroup(appNameSan);
                //migrate SP_app and SP_Idn_Inbound_Auth and SP_Metadata
                int idSp41San = migarateSpApp(appNameSan, username);
                SpApp sp26 = spAppRepository.findFirstByAppNameAndUserName(appNameSan, username);
                migrateIdnInboundAuth(conSumerApps41.getConsumerKey(), idSp41San, sp26.getId());
                migrateSpMetadata(sp26.getId(), idSp41San);
                //migrate UM_HYBRID_ROLE and UM_HYBRID_USER_ROLE
                migrateUmHybridRole(appNameSan,username);
                logger.info("-----done migrate ConsumerApps----. "+appNameSan+"_Username: " +username);
            }
        }
    }

    private void migrateUmHybridRole(String appName, String userName) {
        String roleName = "Application/".concat(appName);
        UmHybridRole umHybridRole = new UmHybridRole();
        umHybridRole.setUmRoleName(roleName);
        umHybridRole.setUmTenantId(-1234);
        umHybridRoleRepository.save(umHybridRole);
        UmHybridRole umHybridRole1 = umHybridRoleRepository.findFirstByUmRoleName(roleName);
        int umId = umHybridRole1.getUmId();

        //save um_hybrid_user_role
        UmHybridUserRole umHybridUserRole = new UmHybridUserRole();
        umHybridUserRole.setUmUserName(userName);
        umHybridUserRole.setUmRoleId(umId);
        umHybridUserRole.setUmTenantId(-1234);
        umHybridUserRole.setUmDomainId(1);
        umHybridUserRoleRepository.save(umHybridUserRole);
    }

    private void migarateIdnScimGroup(String appName) {
        String roleName = "Application/".concat(appName);
        List<IdnScimGroup> list = idnScimGroupRepository.findAllByRoleName(roleName);
        List<IdnScimGroup41> list41 = idnScimGroup41Repository.findAllByRoleName(roleName);
        if (list!= null && list.size() >0 && list41.size() ==0){
            for (IdnScimGroup idnScimGroup26 :list) {
                IdnScimGroup41 idnScimGroup41 = new IdnScimGroup41();
                idnScimGroup41.setTenantId(idnScimGroup26.getTenantId());
                idnScimGroup41.setAttrName(idnScimGroup26.getAttrName());
                idnScimGroup41.setAttrValue(idnScimGroup26.getAttrValue());
                idnScimGroup41.setRoleName(roleName);
                idnScimGroup41Repository.save(idnScimGroup41);
            }
        }

    }

    private void migrateSpMetadata(int idSp26, int idSp41) {
        List<SpMetadata> list = spMetadataRepository.findAllBySpId(idSp26);
        if (list != null && list.size() >0 ){
            for ( SpMetadata spMetadata : list) {
                SpMetadata41 spMetadata41 = new SpMetadata41();
                spMetadata41.setName(spMetadata.getName());
                spMetadata41.setValue(spMetadata.getValue());
                spMetadata41.setTenantId(spMetadata.getTenantId());
                spMetadata41.setDisplayName(spMetadata.getDisplayName());
                spMetadata41.setSpId(idSp41);
                spMetadata41Repository.save(spMetadata41);
            }
        }
    }

    private void migrateIdnInboundAuth(String consumerKey, int idSp41, int idSp26) {
        //list IdnInboundAuth 2.6.0
        List<SpInboundAuth> list = spInboundAuthRepository.findAllByAppIdAndInboundAuthKey(idSp26,consumerKey);
        if (list != null && list.size() > 0){
            for (SpInboundAuth spInboundAuth : list) {
                SpInboundAuth41 spInboundAuth41 = new SpInboundAuth41();
                spInboundAuth41.setTenantId(spInboundAuth.getTenantId());
                spInboundAuth41.setInboundAuthKey(spInboundAuth.getInboundAuthKey());
                spInboundAuth41.setInboundAuthType(spInboundAuth.getInboundAuthType());
                spInboundAuth41.setInboundConfigType(spInboundAuth.getInboundConfigType());
                spInboundAuth41.setPropName(spInboundAuth.getPropName());
                spInboundAuth41.setPropValue(spInboundAuth.getPropValue());
                spInboundAuth41.setAppId(idSp41);
                spInboundAuth41Repository.save(spInboundAuth41);
            }
        }
    }

    private int migarateSpApp(String appNameSan, String userName) {
        //get list at am 2.6.0
        List<SpApp> listSpApp = spAppRepository.findAllByAppNameAndUserName(appNameSan, userName);
        List<SpApp41> listSpApp41 = spApp41Repository.findAllByAppNameAndUserName(appNameSan, userName);
        int spAppId26;
        if (listSpApp != null && listSpApp.size() >0 && listSpApp41.size() == 0){
            SpApp sp = listSpApp.get(0);
            UUID uuid1 = UUID.randomUUID();
            SpApp41 spApp41 = new SpApp41();
            spApp41.setTenantId(sp.getTenantId());
            spApp41.setAppName(sp.getAppName());
            spApp41.setUserStore(sp.getUserStore());
            spApp41.setUserName(sp.getUserName());
            spApp41.setDescription(sp.getDescription());
            spApp41.setRoleClaim(sp.getRoleClaim());
            spApp41.setAuthType(sp.getAuthType());
            spApp41.setProvisioningUserStoreDomain(sp.getProvisioningUserStoreDomain());
            spApp41.setIsLocalClaimDialect(sp.getIsLocalClaimDialect());
            spApp41.setIsSendLocalSubjectId(sp.getIsSendLocalSubjectId());
            spApp41.setIsSendAuthListOfIdps(sp.getIsSendAuthListOfIdps());
            spApp41.setIsUseTenantDomainSubject(sp.getIsUseTenantDomainSubject());
            spApp41.setIsUseUserDomainSubject(sp.getIsUseUserDomainSubject());
            spApp41.setEnableAuthorization(sp.getEnableAuthorization());
            spApp41.setSubjectClaimUri(sp.getSubjectClaimUri());
            spApp41.setIsSaasApp(sp.getIsSaasApp());
            spApp41.setIsDumbMode(sp.getIsDumbMode());
            spApp41.setUuid(String.valueOf(uuid1));
            spApp41.setImageUrl(null);
            spApp41.setAccessUrl(null);
            spApp41.setIsDiscoverable("0");
            spApp41Repository.save(spApp41);
        }
        SpApp41 sp41 = spApp41Repository.findFirstByAppNameAndUserName(appNameSan, userName);
        return sp41.getId();
    }

    private void migrateIdnOidcProduction (String consumerKey){

        List <IdnOidcProperty41> list = new ArrayList<>();
        list.add(new IdnOidcProperty41(consumerKey,-1234,"audience","http://org.wso2.apimgt/gateway"));
        list.add(new IdnOidcProperty41(consumerKey,-1234,"requestObjectSigned","false"));
        list.add(new IdnOidcProperty41(consumerKey,-1234,"idTokenEncrypted","false"));
        list.add(new IdnOidcProperty41(consumerKey,-1234,"idTokenEncryptionAlgorithm","null"));
        list.add(new IdnOidcProperty41(consumerKey,-1234,"idTokenEncryptionMethod","null"));
        list.add(new IdnOidcProperty41(consumerKey,-1234,"backChannelLogoutURL",null));
        list.add(new IdnOidcProperty41(consumerKey,-1234,"tokenType",null));
        list.add(new IdnOidcProperty41(consumerKey,-1234,"bypassClientCredentials","false"));
        for ( IdnOidcProperty41 idnOidcProperty41: list) {
            idnOidcProperty41Repository.save(idnOidcProperty41);
        }

    }

    private void convertIdnOauth2AccessToken (Integer consumerKeyId26, String authzUser, Integer consumerKeyId41){
        List<IdbOauth2AccessToken> listIdbOauth = idbOauth2AccessTokenRepository.findAllByConsumerKeyIdAndAuthzUser(consumerKeyId26, authzUser);
        if (listIdbOauth != null && listIdbOauth.size() > 0){
            for (IdbOauth2AccessToken idbOauth2 : listIdbOauth) {
                IdbOauth2AccessToken41 idb4 = new IdbOauth2AccessToken41();
                idb4.setTokenId(idbOauth2.getTokenId());
                idb4.setAccessToken(idbOauth2.getAccessToken());
                idb4.setRefreshToken(idbOauth2.getRefreshToken());
                idb4.setConsumerKeyId(consumerKeyId41);
                idb4.setAuthzUser(idbOauth2.getAuthzUser());
                idb4.setTenantId(idbOauth2.getTenantId());
                idb4.setUserDomain(idbOauth2.getUserDomain());
                idb4.setUserType(idbOauth2.getUserType());
                idb4.setGrantType(idbOauth2.getGrantType());
                idb4.setTimeCreated(idbOauth2.getTimeCreated());
                idb4.setRefreshTokenTimeCreated(idbOauth2.getRefreshTokenTimeCreated());
                idb4.setValidityPeriod(idbOauth2.getValidityPeriod());
                idb4.setRefreshTokenValitityPeriod(idbOauth2.getRefreshTokenValitityPeriod());
                idb4.setTokenScopeHash(idbOauth2.getTokenScopeHash());
                idb4.setTokenState(idbOauth2.getTokenState());
                idb4.setTokenStateId(idbOauth2.getTokenStateId());
                idb4.setSubjectIdentifier(idbOauth2.getSubjectIdentifier());
                idb4.setAccessTokenHash(idbOauth2.getAccessTokenHash());
                idb4.setRefreshTokenHash(idbOauth2.getRefreshTokenHash());
                idb4.setIdpId(1);
                idb4.setTokenBindingRef("NONE");
                idbOauth2AccessToken41Repository.save(idb4);

                //migrate idn_oauth2_access_token_scope
                migarateScope(idbOauth2.getTokenId());
            }
        }
    }

    private void migarateScope(String tokenId){
        String DB_URL = env.getProperty("db.am.datasource.url") ;
        String USER = env.getProperty("db.am.datasource.username") ;
        final String PASS = env.getProperty("db.am.datasource.password") ;
        String QUERY = "SELECT * FROM IDN_OAUTH2_ACCESS_TOKEN_SCOPE WHERE TOKEN_ID=?";
        List<IdbOauth2AccessTokenScope> listScope = new ArrayList<>();
        try{
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement stmt=conn.prepareStatement(QUERY);
            stmt.setString(1, tokenId);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                IdbOauth2AccessTokenScope idb26 = new IdbOauth2AccessTokenScope();
                idb26.setTokenId(rs.getString("TOKEN_ID"));
                idb26.setTokenScope(rs.getString("TOKEN_SCOPE"));
                idb26.setTenantId(rs.getInt("TENANT_ID"));
                listScope.add(idb26);
            }
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if (listScope != null && listScope.size() >0){
            insertIdbOauth2AccessTokenScope41(listScope);
        }
    }

    private void insertIdbOauth2AccessTokenScope41(List<IdbOauth2AccessTokenScope> listScope) {
        String DB_URL = env.getProperty("db.am41.datasource.url") ;
        String USER = env.getProperty("db.am41.datasource.username") ;
        String PASS = env.getProperty("db.am41.datasource.password");
        String QUERY = "INSERT INTO IDN_OAUTH2_ACCESS_TOKEN_SCOPE(TOKEN_ID, TOKEN_SCOPE, TENANT_ID) VALUES (?,?,?)";
        for (IdbOauth2AccessTokenScope idpScope: listScope) {
            try{
                Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
                PreparedStatement stmt=conn.prepareStatement(QUERY);
                stmt.setString(1, idpScope.getTokenId());
                stmt.setString(2, idpScope.getTokenScope());
                stmt.setInt(3, idpScope.getTenantId());
                stmt.executeUpdate();
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    private void migarateKeyMapping(String consumerKey, String keyType, AmApplication41 app41){
        UUID uuid = UUID.randomUUID();
        String DB_URL = env.getProperty("db.am41.datasource.url");
        String USER = env.getProperty("db.am41.datasource.username") ;
        String PASS = env.getProperty("db.am41.datasource.password");
        String QUERY = "INSERT INTO AM_APPLICATION_KEY_MAPPING(UUID, APPLICATION_ID, CONSUMER_KEY,KEY_TYPE,STATE,CREATE_MODE,KEY_MANAGER) VALUES (?,?,?,?,?,?,?)";
        try{
            Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement stmt=conn.prepareStatement(QUERY);
            stmt.setString(1, String.valueOf(uuid));
            stmt.setInt(2, app41.getApplicationId());
            stmt.setString(3, consumerKey);
            stmt.setString(4, keyType);
            stmt.setString(5, "COMPLETED");
            stmt.setString(6, "CREATED");
            stmt.setString(7, env.getProperty("keyManager"));
            stmt.executeUpdate();
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void migrateApplicationRegistration(AmApplication41 app41, AmApplication app26) {
        //list registration 2.6
        List<ApplicationRegistration> listRegistration26 = applicationRegistrationRepository.findAllByAppIdAndSubscriberId(app26.getApplicationId(), app26.getSubscriberId());
        if (listRegistration26!= null && listRegistration26.size()>0){
            for (ApplicationRegistration registration:listRegistration26) {
                ApplicationRegistration41  registration4 = applicationRegistration41Repository.findFirstBySubscriberIdAndTokenTypeAndAppId(app41.getSubscriberId(),registration.getTokenType(),app41.getApplicationId());
                if (registration4 != null){
                    continue;
                }else {
                    ApplicationRegistration41 registration41 = new ApplicationRegistration41();
                    registration41.setSubscriberId(app41.getSubscriberId());
                    registration41.setWfRef(registration.getWfRef());
                    registration41.setAppId(app41.getApplicationId());
                    registration41.setTokenType(registration.getTokenType());
                    registration41.setTokenScope(registration.getTokenScope());
                    registration41.setInputs(registration.getInputs());
                    registration41.setAllowedDomains(registration.getAllowedDomains());
                    registration41.setValidityPeriod(registration.getValidityPeriod());
                    registration41.setKeyManager(env.getProperty("keyManager"));
                    applicationRegistration41Repository.save(registration41);
                }
            }
        }
    }

    private void migrateSubcription(int app26Id, int app41Id) {
        List<Subscription> subscriptions = subscriptionRepository.findAllByApplicationId(app26Id);
        if (subscriptions !=null && subscriptions.size() >0){
            for (Subscription sub: subscriptions) {
                //lay thong tin chi tiet api da sub 2.6.0
                AmApi api26 = amApiRepository.findByApiId(sub.getApiId());
                if (api26 != null) {
                    //check API exit 4.1.0 neu co thi sub. chua co thi bo qua
                    AmApi41 amApi41 = amApi41Repository.findByApiNameAndApiVersionAndApiProvider(api26.getApiName(), api26.getApiVersion(), api26.getApiProvider());
                    if (amApi41!= null){
                        Subscription41 subscription41 = new Subscription41();
                        subscription41.setTierId(sub.getTierId());
                        subscription41.setTierIdPending(sub.getTierId());
                        subscription41.setApiId(amApi41.getApiId());
                        subscription41.setLastAccessed(sub.getLastAccessed());
                        subscription41.setApplicationId(app41Id);
                        subscription41.setSubStatus(sub.getSubStatus());
                        subscription41.setSubStatusState(sub.getSubStatusState());
                        subscription41.setCreatedBy(sub.getCreatedBy());
                        subscription41.setCreatedTime(sub.getCreatedTime());
                        subscription41.setUpdateBy(sub.getUpdateBy());
                        subscription41.setUpdatedTime(sub.getUpdatedTime());
                        subscription41.setUuid(sub.getUuid());
                        subscription41Repository.save(subscription41);
                    }
                }
            }
        }
    }

    private ConSumerApps41 convertConSumerApps(ConSumerApps conSumerApps , ConSumerApps41 conSumerApps41){
        conSumerApps41.setConsumerKey(conSumerApps.getConsumerKey());
        conSumerApps41.setConsumerSecret(conSumerApps.getConsumerSecret());
        conSumerApps41.setUserName(conSumerApps.getUserName());
        conSumerApps41.setTenantId(conSumerApps.getTenantId());
        conSumerApps41.setUserDomain(conSumerApps.getUserDomain());
        conSumerApps41.setAppName(conSumerApps.getAppName());
        conSumerApps41.setOauthVersion(conSumerApps.getOauthVersion());
        conSumerApps41.setCallBackUrl(conSumerApps.getCallBackUrl());
        conSumerApps41.setGrantTypes(conSumerApps.getGrantTypes());
        conSumerApps41.setPkceMandatory(conSumerApps.getPkceMandatory());
        conSumerApps41.setPkceSupportPlain(conSumerApps.getPkceSupportPlain());
        conSumerApps41.setAppState(conSumerApps.getAppState());
        conSumerApps41.setUserExpireTime(conSumerApps.getUserExpireTime());
        conSumerApps41.setAppExpireTime(conSumerApps.getAppExpireTime());
        conSumerApps41.setRefreshExpireTime(conSumerApps.getRefreshExpireTime());
        conSumerApps41.setIdExpireTime(conSumerApps.getIdExpireTime());
        return conSumerApps41;
    }
}
