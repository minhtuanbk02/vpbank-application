package vpbank.com.application26.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application26.entity.SpMetadata;

import java.util.List;

public interface SpMetadataRepository extends JpaRepository<SpMetadata, Integer>, JpaSpecificationExecutor<SpMetadata> {
    List<SpMetadata> findAllBySpId(Integer spId);
}
