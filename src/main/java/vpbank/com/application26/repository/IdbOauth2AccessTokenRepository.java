package vpbank.com.application26.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application26.entity.ConSumerApps;
import vpbank.com.application26.entity.IdbOauth2AccessToken;

import java.util.List;

public interface IdbOauth2AccessTokenRepository extends JpaRepository<IdbOauth2AccessToken, Integer>, JpaSpecificationExecutor<IdbOauth2AccessToken> {
    List<IdbOauth2AccessToken> findIdbOauth2AccessTokensByConsumerKeyIdAndAuthzUser(Integer consumerKeyId, String authzUser);

    List<IdbOauth2AccessToken> findAllByConsumerKeyIdAndAuthzUser(Integer consumerKeyId, String authzUser);

}
