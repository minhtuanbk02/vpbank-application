package vpbank.com.application26.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application26.entity.SpApp;

import java.util.List;

public interface SpAppRepository extends JpaRepository<SpApp, Integer>, JpaSpecificationExecutor<SpApp> {
    List<SpApp> findAllByAppNameAndUserName(String appName, String userName);
    SpApp findFirstByAppNameAndUserName(String appName, String userName);
}
