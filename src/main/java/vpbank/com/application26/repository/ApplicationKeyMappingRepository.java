package vpbank.com.application26.repository;

import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import vpbank.com.application26.entity.ApplicationKeyMapping;

import java.util.List;

public interface ApplicationKeyMappingRepository extends JpaRepository<ApplicationKeyMapping, Integer>, JpaSpecificationExecutor<ApplicationKeyMapping> {
   List<ApplicationKeyMapping> findAllByApplicationIdAndConsumerKeyAndKeyType(Integer applicationId, String consumerKey, String keyType);

   @Query("SELECT u FROM ApplicationKeyMapping u WHERE u.consumerKey = ?1 and u.keyType = ?2")
   List<ApplicationKeyMapping> findAllByConsumerKeyAndKeyType(String consumerKey, String keyType);

}
