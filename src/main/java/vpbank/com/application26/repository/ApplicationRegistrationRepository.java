package vpbank.com.application26.repository;

import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vpbank.com.application26.entity.ApplicationRegistration;
import vpbank.com.application41.entity.ApplicationRegistration41;

import java.util.List;

@Repository
public interface ApplicationRegistrationRepository extends JpaRepository<ApplicationRegistration, Integer>, JpaSpecificationExecutor<ApplicationRegistration> {

    ApplicationRegistration findFirstByAppIdAndTokenTypeAndSubscriberId(Integer appId, String tokenType, Integer subscriberId);

    List<ApplicationRegistration> findAllByAppIdAndSubscriberId(Integer appId, Integer subscriberId);
}
