package vpbank.com.application26.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application26.entity.SpInboundAuth;

import java.util.List;

public interface SpInboundAuthRepository extends JpaRepository<SpInboundAuth, Integer>, JpaSpecificationExecutor<SpInboundAuth> {
    List<SpInboundAuth> findAllByAppIdAndInboundAuthKey(Integer appId, String key);
}
