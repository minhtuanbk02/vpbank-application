package vpbank.com.application26.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application26.entity.ConSumerApps;

import java.util.List;

public interface ConsumerAppsRepository extends JpaRepository<ConSumerApps, Integer>, JpaSpecificationExecutor<ConSumerApps> {
    List<ConSumerApps> findConSumerAppsByAppNameAndUserName(String appName, String userName);

    List<ConSumerApps> findAllByAppNameAndUserName(String appName, String userName);
}
