package vpbank.com.application26.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application26.entity.Subcriber;

public interface SubscriberRepository extends JpaRepository<Subcriber, Integer>, JpaSpecificationExecutor<Subcriber> {
    Subcriber findSubcriberBySubscriberId(int subscriberId);
}
