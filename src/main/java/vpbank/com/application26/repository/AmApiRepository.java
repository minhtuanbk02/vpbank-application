package vpbank.com.application26.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vpbank.com.application26.entity.AmApi;

@Repository
public interface AmApiRepository extends JpaRepository<AmApi, Integer>, JpaSpecificationExecutor<AmApi> {
    AmApi findByApiNameAndApiVersionAndApiProvider(String apiName, String apiVersion, String apiProvider);
    AmApi findByApiId(Integer id);

}
