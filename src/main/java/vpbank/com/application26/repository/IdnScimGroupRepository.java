package vpbank.com.application26.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application26.entity.IdnScimGroup;
import vpbank.com.application26.entity.SpMetadata;

import java.util.List;

public interface IdnScimGroupRepository extends JpaRepository<IdnScimGroup, Integer>, JpaSpecificationExecutor<IdnScimGroup> {
    List<IdnScimGroup> findAllByRoleName(String roleName);
}
