package vpbank.com.application26.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vpbank.com.application26.entity.AmApplication;

import java.util.Date;
import java.util.List;

@Repository
public interface AmApplicationRepository extends JpaRepository<AmApplication, Integer>, JpaSpecificationExecutor<AmApplication> {

    AmApplication findByApplicationId(int applicationId);

    @Query("select aa from AmApplication aa where aa.createdTime >= ?1")
    List<AmApplication> findAmApplicationByCreatedTimeGreaterThanEqual (Date createdTime);

    AmApplication findFirstByNameAndCreatedBy(String appName, String userName);

}
