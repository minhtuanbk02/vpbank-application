package vpbank.com.application26.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import vpbank.com.application26.entity.IdbOauth2AccessTokenScope;

import java.util.List;

public interface IdbOauth2AccessTokenScopeRepository extends JpaRepository<IdbOauth2AccessTokenScope, Integer>, JpaSpecificationExecutor<IdbOauth2AccessTokenScope> {
    List<IdbOauth2AccessTokenScope> findByTokenId(String tokenId);

    @Query("SELECT idb FROM IdbOauth2AccessTokenScope idb WHERE idb.tokenId = ?1 and idb.tenantId = ?2")
    List<IdbOauth2AccessTokenScope> findAllByTokenIdAndTenantId(String tokenId, Integer tenantId);
}
