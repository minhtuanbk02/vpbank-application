package vpbank.com.application26.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "AM_APPLICATION_KEY_MAPPING")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ApplicationKeyMapping implements Serializable {

    @Id
    @Column(name = "APPLICATION_ID")
    private Integer applicationId;

    @Column(name = "CONSUMER_KEY")
    private String consumerKey;

    @Column(name = "KEY_TYPE")
    private String keyType;

    @Column(name = "STATE")
    private String state;

    @Column(name = "CREATE_MODE")
    private String createMode;
}
