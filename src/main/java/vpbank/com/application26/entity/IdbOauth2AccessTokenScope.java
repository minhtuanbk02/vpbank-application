package vpbank.com.application26.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "IDN_OAUTH2_ACCESS_TOKEN_SCOPE")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IdbOauth2AccessTokenScope implements Serializable {

    @Id
    @Column(name = "TOKEN_ID")
    private String tokenId;

    @Column(name = "TOKEN_SCOPE")
    private String tokenScope;

    @Column(name = "TENANT_ID")
    private Integer tenantId;
}
