package vpbank.com.application26.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "AM_APPLICATION_REGISTRATION")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ApplicationRegistration implements Serializable {

    @Id
    @Column(name = "REG_ID")
    private Integer regId;

    @Column(name = "SUBSCRIBER_ID")
    private Integer subscriberId;

    @Column(name = "WF_REF")
    private String wfRef;

    @Column(name = "APP_ID")
    private Integer appId;

    @Column(name = "TOKEN_TYPE")
    private String tokenType;

    @Column(name = "TOKEN_SCOPE")
    private String tokenScope;

    @Column(name = "INPUTS")
    private String inputs;

    @Column(name = "ALLOWED_DOMAINS")
    private String allowedDomains;

    @Column(name = "VALIDITY_PERIOD")
    private Integer validityPeriod;
}
