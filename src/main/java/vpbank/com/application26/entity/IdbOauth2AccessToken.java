package vpbank.com.application26.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "IDN_OAUTH2_ACCESS_TOKEN")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IdbOauth2AccessToken implements Serializable {

    @Id
    @Column(name = "TOKEN_ID")
    private String tokenId;

    @Column(name = "ACCESS_TOKEN")
    private String accessToken;

    @Column(name = "REFRESH_TOKEN")
    private String refreshToken;

    @Column(name = "CONSUMER_KEY_ID")
    private Integer consumerKeyId;

    @Column(name = "AUTHZ_USER")
    private String authzUser;

    @Column(name = "TENANT_ID")
    private Integer tenantId;

    @Column(name = "USER_DOMAIN")
    private String userDomain;

    @Column(name = "USER_TYPE")
    private String userType;

    @Column(name = "GRANT_TYPE")
    private String grantType;

    @Column(name = "TIME_CREATED")
    private Date timeCreated;

    @Column(name = "REFRESH_TOKEN_TIME_CREATED")
    private Date refreshTokenTimeCreated;

    @Column(name = "VALIDITY_PERIOD")
    private Double validityPeriod;

    @Column(name = "REFRESH_TOKEN_VALIDITY_PERIOD")
    private Double refreshTokenValitityPeriod;

    @Column(name = "TOKEN_SCOPE_HASH")
    private String tokenScopeHash;

    @Column(name = "TOKEN_STATE")
    private String tokenState;

    @Column(name = "TOKEN_STATE_ID")
    private String tokenStateId;

    @Column(name = "SUBJECT_IDENTIFIER")
    private String subjectIdentifier;

    @Column(name = "ACCESS_TOKEN_HASH")
    private String accessTokenHash;

    @Column(name = "REFRESH_TOKEN_HASH")
    private String refreshTokenHash;
}
