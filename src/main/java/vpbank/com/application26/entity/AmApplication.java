package vpbank.com.application26.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "AM_APPLICATION")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AmApplication implements Serializable {

    @Id
    @Column(name = "APPLICATION_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer applicationId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SUBSCRIBER_ID")
    private Integer subscriberId;

    @Column(name = "APPLICATION_TIER")
    private String applicationTier;

    @Column(name = "CALLBACK_URL")
    private String callbackUrl;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "APPLICATION_STATUS")
    private String applicationStatus;

    @Column(name = "GROUP_ID")
    private String groupId;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_TIME")
    private Date createdTime;

    @Column(name = "UPDATED_BY")
    private String updateBy;

    @Column(name = "UPDATED_TIME")
    private Date updatedTime;

    @Column(name = "UUID")
    private String uuid;

    @Column(name = "TOKEN_TYPE")
    private String tokenType;

}
