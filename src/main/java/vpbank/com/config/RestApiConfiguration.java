package vpbank.com.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class RestApiConfiguration implements WebMvcConfigurer {

    @Value("${wb.address}")
    private String webAddress;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry
                .addMapping("/**")
                .allowedOrigins(webAddress.split("\\s*,\\s*"))
                .allowedMethods("GET", "HEAD", "POST", "PUT", "PATCH", "DELETE")
                .allowedHeaders("Origin", "Access-Control-Allow-Origin",
                        "Access-Control-Allow-Headers", "Content-Type", "Authorization", "X-Requested-With",
                        "Accept", "X-Requested-With", "remember-me", "authorization", "x-auth-token",
                        "reportProgress", "Access-Control-Request-Method", "Access-Control-Request-Headers")
                .allowCredentials(false)
                .maxAge(3600L)
        ;
    }

}
