package vpbank.com;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = {
                "vpbank.com.application41.repository",
                "vpbank.com.application41.entity"
        },
        entityManagerFactoryRef = "amDB41EntityManagerFactory",
        transactionManagerRef = "amDB41TransactionManager"
)
public class AmDB41Configuration {
    @Autowired
    private Environment env;
    private String dbPrifix = "db.am41.datasource";
    private String packagePrifix = "vpbank.com.application41";

    @Bean
    public LocalContainerEntityManagerFactoryBean amDB41EntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(amDB41DataSource());
        em.setPackagesToScan(new String[] {
                packagePrifix + ".entity"
        });

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setPersistenceUnitName(dbPrifix);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty(dbPrifix + ".dll-auto"));
        properties.put("hibernate.dialect", env.getProperty(dbPrifix + ".dialect"));
        properties.put("hibernate.show_sql", env.getProperty(dbPrifix + ".show_sql"));
        properties.put("hibernate.enable_lazy_load_no_trans", true);
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean
    public DataSource amDB41DataSource() {
        BasicDataSource bds = new BasicDataSource();
        bds.setDriverClassName(env.getProperty(dbPrifix + ".driverClassName"));
        bds.setUrl(env.getProperty(dbPrifix + ".url"));
        bds.setUsername(env.getProperty(dbPrifix + ".username"));
        bds.setPassword(env.getProperty(dbPrifix + ".password"));
        return bds;
    }

    @Bean
    public PlatformTransactionManager amDB41TransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(amDB41EntityManagerFactory().getObject());
        return transactionManager;
    }

}
