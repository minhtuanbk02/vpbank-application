package vpbank.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VpbankApplication {

    public static void main(String[] args) {
        SpringApplication.run(VpbankApplication.class, args);
    }

}
