package vpbank.com.shared41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vpbank.com.shared41.entity.UmHybridUserRole;


@Repository
public interface UmHybridUserRoleRepository extends JpaRepository<UmHybridUserRole, Integer>, JpaSpecificationExecutor<UmHybridUserRole> {


}
