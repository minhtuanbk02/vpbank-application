package vpbank.com.shared41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vpbank.com.shared41.entity.UmHybridRole;

@Repository
public interface UmHybridRoleRepository extends JpaRepository<UmHybridRole, Integer>, JpaSpecificationExecutor<UmHybridRole> {
    UmHybridRole findFirstByUmRoleName(String UmRoleName);

}
