package vpbank.com.shared41.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "UM_HYBRID_ROLE")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UmHybridRole implements Serializable {

    @Id
    @Column(name = "UM_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer umId;

    @Column(name = "UM_ROLE_NAME")
    private String umRoleName;

    @Column(name = "UM_TENANT_ID")
    private Integer umTenantId;
}
