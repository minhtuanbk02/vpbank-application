package vpbank.com.shared41.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "UM_HYBRID_USER_ROLE")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UmHybridUserRole implements Serializable {
    @Id
    @Column(name = "UM_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer umId;

    @Column(name = "UM_USER_NAME")
    private String umUserName;

    @Column(name = "UM_ROLE_ID")
    private Integer umRoleId;

    @Column(name = "UM_TENANT_ID")
    private Integer umTenantId;

    @Column(name = "UM_DOMAIN_ID")
    private Integer umDomainId;

}
