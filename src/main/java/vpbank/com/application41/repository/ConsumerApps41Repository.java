package vpbank.com.application41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application26.entity.ConSumerApps;
import vpbank.com.application41.entity.ConSumerApps41;

import java.util.List;

public interface ConsumerApps41Repository extends JpaRepository<ConSumerApps41, Integer>, JpaSpecificationExecutor<ConSumerApps41> {

    List<ConSumerApps41> findConSumerAppsByAppNameAndUserName(String appName, String userName);

    List<ConSumerApps41> findAllByAppNameAndUserName(String appName, String userName);
}
