package vpbank.com.application41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application26.entity.SpApp;
import vpbank.com.application41.entity.SpApp41;

import java.util.List;

public interface SpApp41Repository extends JpaRepository<SpApp41, Integer>, JpaSpecificationExecutor<SpApp41> {

    List<SpApp41> findAllByAppNameAndUserName(String appNameSan, String userName);

    SpApp41 findFirstByAppNameAndUserName(String appNameSan, String userName);

    List<SpApp41> findAllByAppNameAndUserNameAndUuid(String appNameSan, String userName, String uuid);
}
