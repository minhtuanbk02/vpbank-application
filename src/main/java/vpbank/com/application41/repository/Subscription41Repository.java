package vpbank.com.application41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application41.entity.Subscription41;

import java.util.List;

public interface Subscription41Repository extends JpaRepository<Subscription41, Integer>, JpaSpecificationExecutor<Subscription41> {

}
