package vpbank.com.application41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vpbank.com.application41.entity.AmApplication41;

import java.util.List;

@Repository
public interface AmApplication41Repository extends JpaRepository<AmApplication41, Integer>, JpaSpecificationExecutor<AmApplication41> {

    AmApplication41 findByApplicationId(int applicationId);

    List<AmApplication41> findAmApplication41sByNameAndCreatedBy(String name, String createBy);
    AmApplication41 findFirstByNameAndCreatedBy(String appName, String userName);

}
