package vpbank.com.application41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application41.entity.IdnScimGroup41;

import java.util.List;

public interface IdnScimGroup41Repository extends JpaRepository<IdnScimGroup41, Integer>, JpaSpecificationExecutor<IdnScimGroup41> {
    List<IdnScimGroup41> findAllByRoleName(String roleName);
}
