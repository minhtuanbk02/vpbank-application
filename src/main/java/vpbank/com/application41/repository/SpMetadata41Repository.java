package vpbank.com.application41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application41.entity.SpMetadata41;

import java.util.List;

public interface SpMetadata41Repository extends JpaRepository<SpMetadata41, Integer>, JpaSpecificationExecutor<SpMetadata41> {
    List<SpMetadata41> findAllBySpId(Integer spId);
}
