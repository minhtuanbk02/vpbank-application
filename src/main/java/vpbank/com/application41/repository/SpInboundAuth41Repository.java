package vpbank.com.application41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application26.entity.SpInboundAuth;
import vpbank.com.application41.entity.SpInboundAuth41;

import java.util.List;

public interface SpInboundAuth41Repository extends JpaRepository<SpInboundAuth41, Integer>, JpaSpecificationExecutor<SpInboundAuth41> {
}
