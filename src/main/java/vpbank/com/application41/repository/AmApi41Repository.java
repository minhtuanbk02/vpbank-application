package vpbank.com.application41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vpbank.com.application41.entity.AmApi41;

@Repository
public interface AmApi41Repository extends JpaRepository<AmApi41, Integer>, JpaSpecificationExecutor<AmApi41> {
    AmApi41 findByApiUuid(String apiUuid);
    AmApi41 findByApiNameAndApiVersion(String apiName, String apiVersion);
    AmApi41 findByApiNameAndApiVersionAndApiProvider(String apiName, String apiVersion, String apiProvider);
}
