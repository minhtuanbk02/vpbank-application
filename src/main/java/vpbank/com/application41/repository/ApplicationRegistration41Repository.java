package vpbank.com.application41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import vpbank.com.application41.entity.ApplicationRegistration41;

import java.util.List;

@Repository
public interface ApplicationRegistration41Repository extends JpaRepository<ApplicationRegistration41, Integer>, JpaSpecificationExecutor<ApplicationRegistration41> {

    ApplicationRegistration41 findFirstBySubscriberIdAndTokenTypeAndAppId(Integer subId, String tokenType, Integer appId);
}
