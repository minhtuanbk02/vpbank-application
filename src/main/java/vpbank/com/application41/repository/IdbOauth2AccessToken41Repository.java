package vpbank.com.application41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application26.entity.IdbOauth2AccessToken;
import vpbank.com.application41.entity.IdbOauth2AccessToken41;

import java.util.List;

public interface IdbOauth2AccessToken41Repository extends JpaRepository<IdbOauth2AccessToken41, Integer>, JpaSpecificationExecutor<IdbOauth2AccessToken41> {
    List<IdbOauth2AccessToken41> findIdbOauth2AccessTokensByConsumerKeyIdAndAuthzUser(Integer consumerKeyId, String authzUser);
}
