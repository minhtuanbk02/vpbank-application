package vpbank.com.application41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application41.entity.Subcriber41;

import java.util.List;

public interface Subscriber41Repository extends JpaRepository<Subcriber41, Integer>, JpaSpecificationExecutor<Subcriber41> {
    List<Subcriber41> findSubcriber41sByUserId(String userId);

    List<Subcriber41> findAllByUserId(String userId);
}
