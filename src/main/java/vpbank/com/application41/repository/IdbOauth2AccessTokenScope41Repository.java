package vpbank.com.application41.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import vpbank.com.application41.entity.IdbOauth2AccessTokenScope41;

public interface IdbOauth2AccessTokenScope41Repository extends JpaRepository<IdbOauth2AccessTokenScope41, String>, JpaSpecificationExecutor<IdbOauth2AccessTokenScope41> {
}
