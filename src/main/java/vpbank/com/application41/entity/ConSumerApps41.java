package vpbank.com.application41.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "IDN_OAUTH_CONSUMER_APPS")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ConSumerApps41 implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "CONSUMER_KEY")
    private String consumerKey;

    @Column(name = "CONSUMER_SECRET")
    private String consumerSecret;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "TENANT_ID")
    private Integer tenantId;

    @Column(name = "USER_DOMAIN")
    private String userDomain;

    @Column(name = "APP_NAME")
    private String appName;

    @Column(name = "OAUTH_VERSION")
    private String oauthVersion;

    @Column(name = "CALLBACK_URL")
    private String callBackUrl;

    @Column(name = "GRANT_TYPES")
    private String grantTypes;

    @Column(name = "PKCE_MANDATORY")
    private String pkceMandatory;

    @Column(name = "PKCE_SUPPORT_PLAIN")
    private String pkceSupportPlain;

    @Column(name = "APP_STATE")
    private String appState;

    @Column(name = "USER_ACCESS_TOKEN_EXPIRE_TIME")
    private Double userExpireTime;

    @Column(name = "APP_ACCESS_TOKEN_EXPIRE_TIME")
    private Double appExpireTime;

    @Column(name = "REFRESH_TOKEN_EXPIRE_TIME")
    private Double refreshExpireTime;

    @Column(name = "ID_TOKEN_EXPIRE_TIME")
    private Double idExpireTime;
}
