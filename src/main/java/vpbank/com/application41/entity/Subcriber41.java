package vpbank.com.application41.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "AM_SUBSCRIBER")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Subcriber41 implements Serializable {

    @Id
    @Column(name = "SUBSCRIBER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer subscriberId;

    @Column(name = "USER_ID")
    private String userId;

    @Column(name = "TENANT_ID")
    private Integer tenantId;

    @Column(name = "EMAIL_ADDRESS")
    private String emailAddress;

    @Column(name = "DATE_SUBSCRIBED")
    private String dateSubscribed;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_TIME")
    private Date createdTime;

    @Column(name = "UPDATED_BY")
    private String updateBy;

    @Column(name = "UPDATED_TIME")
    private Date updatedTime;
}
