package vpbank.com.application41.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "IDN_SCIM_GROUP")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IdnScimGroup41 implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(name = "TENANT_ID")
    private Integer tenantId;

    @Column(name = "ROLE_NAME")
    private String roleName;

    @Column(name = "ATTR_NAME")
    private String attrName;

    @Column(name = "ATTR_VALUE")
    private String attrValue;

}
