package vpbank.com.application41.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "SP_APP")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SpApp41 implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(name = "TENANT_ID")
    private Integer tenantId;

    @Column(name = "APP_NAME")
    private String appName;

    @Column(name = "USER_STORE")
    private String userStore;

    @Column(name = "USERNAME")
    private String userName;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "ROLE_CLAIM")
    private String roleClaim;

    @Column(name = "AUTH_TYPE")
    private String authType;

    @Column(name = "PROVISIONING_USERSTORE_DOMAIN")
    private String provisioningUserStoreDomain;

    @Column(name = "IS_LOCAL_CLAIM_DIALECT")
    private String isLocalClaimDialect;

    @Column(name = "IS_SEND_LOCAL_SUBJECT_ID")
    private String isSendLocalSubjectId;

    @Column(name = "IS_SEND_AUTH_LIST_OF_IDPS")
    private String isSendAuthListOfIdps;

    @Column(name = "IS_USE_TENANT_DOMAIN_SUBJECT")
    private String isUseTenantDomainSubject;

    @Column(name = "IS_USE_USER_DOMAIN_SUBJECT")
    private String isUseUserDomainSubject;

    @Column(name = "ENABLE_AUTHORIZATION")
    private String enableAuthorization;

    @Column(name = "SUBJECT_CLAIM_URI")
    private String subjectClaimUri;

    @Column(name = "IS_SAAS_APP")
    private String isSaasApp;

    @Column(name = "IS_DUMB_MODE")
    private String isDumbMode;

    @Column(name = "UUID")
    private String uuid;

    @Column(name = "IMAGE_URL")
    private String imageUrl;

    @Column(name = "ACCESS_URL")
    private String accessUrl;

    @Column(name = "IS_DISCOVERABLE")
    private String isDiscoverable;

}
