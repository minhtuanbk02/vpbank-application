package vpbank.com.application41.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "SP_INBOUND_AUTH")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SpInboundAuth41 implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(name = "TENANT_ID")
    private Integer tenantId;

    @Column(name = "INBOUND_AUTH_KEY")
    private String inboundAuthKey;

    @Column(name = "INBOUND_AUTH_TYPE")
    private String inboundAuthType;

    @Column(name = "INBOUND_CONFIG_TYPE")
    private String inboundConfigType;

    @Column(name = "PROP_NAME")
    private String propName;

    @Column(name = "PROP_VALUE")
    private String propValue;

    @Column(name = "APP_ID")
    private Integer appId;

}
