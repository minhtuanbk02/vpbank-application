package vpbank.com.application41.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "AM_APPLICATION_REGISTRATION")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ApplicationRegistration41 implements Serializable {

    @Id
    @Column(name = "REG_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer regId;

    @Column(name = "SUBSCRIBER_ID")
    private Integer subscriberId;

    @Column(name = "WF_REF")
    private String wfRef;

    @Column(name = "APP_ID")
    private Integer appId;

    @Column(name = "TOKEN_TYPE")
    private String tokenType;

    @Column(name = "TOKEN_SCOPE")
    private String tokenScope;

    @Column(name = "INPUTS")
    private String inputs;

    @Column(name = "ALLOWED_DOMAINS")
    private String allowedDomains;

    @Column(name = "VALIDITY_PERIOD")
    private Integer validityPeriod;

    @Column(name = "KEY_MANAGER")
    private String keyManager;
}
