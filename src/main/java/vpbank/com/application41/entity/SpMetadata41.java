package vpbank.com.application41.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "SP_METADATA")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SpMetadata41 implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(name = "SP_ID")
    private Integer spId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "DISPLAY_NAME")
    private String displayName;

    @Column(name = "TENANT_ID")
    private Integer tenantId;
}
