package vpbank.com.application41.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "AM_SUBSCRIPTION")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Subscription41 implements Serializable {

    @Id
    @Column(name = "SUBSCRIPTION_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer subscriptionId;

    @Column(name = "TIER_ID")
    private String tierId;

    @Column(name = "TIER_ID_PENDING")
    private String tierIdPending;

    @Column(name = "API_ID")
    private Integer apiId;

    @Column(name = "LAST_ACCESSED")
    private Date lastAccessed;

    @Column(name = "APPLICATION_ID")
    private Integer applicationId;

    @Column(name = "SUB_STATUS")
    private String subStatus;

    @Column(name = "SUBS_CREATE_STATE")
    private String subStatusState;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_TIME")
    private Date createdTime;

    @Column(name = "UPDATED_BY")
    private String updateBy;

    @Column(name = "UPDATED_TIME")
    private Date updatedTime;

    @Column(name = "UUID")
    private String uuid;
}
