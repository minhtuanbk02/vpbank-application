package vpbank.com.application41.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "IDN_OAUTH2_ACCESS_TOKEN_SCOPE")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IdbOauth2AccessTokenScope41 implements Serializable {

    @Id
    @Column(name = "TOKEN_ID")
    private String tokenId;

    @Column(name = "TOKEN_SCOPE")
    private String tokenScope;

    @Column(name = "TENANT_ID")
    private Integer tenantId;
}
