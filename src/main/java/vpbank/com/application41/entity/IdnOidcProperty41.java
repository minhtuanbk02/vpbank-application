package vpbank.com.application41.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "IDN_OIDC_PROPERTY")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class IdnOidcProperty41 implements Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "CONSUMER_KEY")
    private String consumerKey;

    @Column(name = "TENANT_ID")
    private Integer tenantId;

    @Column(name = "PROPERTY_KEY")
    private String propertyKey;

    @Column(name = "PROPERTY_VALUE")
    private String propertyValue;


    public IdnOidcProperty41() {
    }

    public IdnOidcProperty41( String consumerKey, Integer tenantId, String propertyKey, String propertyValue) {
        this.consumerKey = consumerKey;
        this.tenantId = tenantId;
        this.propertyKey = propertyKey;
        this.propertyValue = propertyValue;
    }
}
