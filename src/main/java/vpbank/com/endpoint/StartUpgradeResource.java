package vpbank.com.endpoint;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vpbank.com.common.ResponseData;
import vpbank.com.common.Result;
import vpbank.com.service.ApplicationService;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class StartUpgradeResource {

    @Autowired
    ApplicationService applicationService;
    @Value("${dateStart}") private String dateStart;

    @Value("${appName}") private String appName;

    @GetMapping(value = "/start-upgrade")
    @Bean
    public ResponseEntity<ResponseData> startUpgradeData() {
        String dateStart1 = dateStart;
        applicationService.migrateApplication(dateStart1, appName);
        return new ResponseEntity(new ResponseData("Thành công", Result.SUCCESS_UPGRADE), HttpStatus.OK);
    }

}
